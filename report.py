#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import psycopg2


def most_popular_articles(cursor, num_articles=3):
    """
    return rows containing three most popular articles of all time
    (desc order). First column is article title, second column is
    number of views
    """
    query = """
    SELECT
        articles.title,
        article_views.views
    FROM
        articles,
        article_views
    WHERE
        articles.id = article_views.id
    ORDER BY
        article_views.views DESC
    LIMIT %(limit)s;
    """
    # execute query and return rows
    cursor.execute(query, {'limit': num_articles})
    rows = cursor.fetchall()
    return rows


def most_popular_articles_readable(cursor, num_articles=3):
    """
    Returns report answering following question.
    1. What are the most popular three articles of all time? Which articles
    have been accessed the most? Present this information as a sorted list
    with the most popular article at the top.
        Example:
            "Princess Shellfish Marries Prince Handsome" — 1201 views
            "Baltimore Ravens Defeat Rhode Island Shoggoths" — 915 views
            "Political Scandal Ends In Political Scandal" — 553 views
    """
    # get rows containing most popular articles
    articles_rows = most_popular_articles(cursor, num_articles)
    # make rows more readable
    readable_articles_rows = ['"{title}" — {views} views'.format(
        title=row[0], views=row[1]) for row in articles_rows]
    # return report with title
    report = "What are the most popular three articles of all time? Which " \
        "articles have been accessed the most?\n"
    report += "\n".join(readable_articles_rows)
    return report


def most_popular_authors(cursor):
    """
    return rows containing information about the most popular article authors.
    First column is author, second column is how many views their pages have
    gotten (descending order).
    """
    query = """
    SELECT
        authors.name,
        sum(article_views.views)
    FROM
        authors,
        articles,
        article_views
    WHERE
        authors.id = articles.author
        AND articles.id = article_views.id
    GROUP BY
        authors.id;
    """
    cursor.execute(query)
    rows = cursor.fetchall()
    return rows


def most_popular_authors_readable(cursor):
    """
    2. Who are the most popular article authors of all time? That is, when
    you sum up all of the articles each author has written, which authors
    get the most page views? Present this as a sorted list with the most
    popular author at the top.
        Example:
            Ursula La Multa — 2304 views
            Rudolf von Treppenwitz — 1985 views
            Markoff Chaney — 1723 views
            Anonymous Contributor — 1023 views
    """
    # get rows containing most popular authors
    authors_rows = most_popular_authors(cursor)
    # make rows more readable
    readable_authors_rows = ['{author} — {views} views'.format(
        author=row[0], views=row[1]) for row in authors_rows]
    # return report with title
    report = "Who are the most popular article authors of all time?\n"
    report += "\n".join(readable_authors_rows)
    return report


def many_errors_days(cursor):
    query = """
    SELECT
        num_requests_day_logs.day,
        (1.0 * num_errors / num_requests) AS error_percentage
    FROM
        num_requests_day_logs,
        num_errors_day_logs
    WHERE
        num_requests_day_logs.day = num_errors_day_logs.day
        AND (1.0 * num_errors / num_requests) > 0.01;
    """
    cursor.execute(query)
    rows = cursor.fetchall()
    return rows


def many_errors_days_readable(cursor):
    """
    3. On which days did more than 1% of requests lead to errors? The log
    table includes a column status that indicates the HTTP status code that
    the news site sent to the user's browser.
        Example:
            July 29, 2016 — 2.5% errors
    """
    # get rows containing days with many errors
    days_rows = many_errors_days(cursor)
    # make rows more readable
    readable_days_rows = ['{day} — {error_percent:.1%}'.format(
        day=row[0].strftime('%B %d, %Y'), error_percent=row[1]
    )
        for row in days_rows]
    # return report with title
    report = "On which days did more than 1%% of requests lead to errors?\n"
    report += "\n".join(readable_days_rows)
    return report


def generated_report(cursor):
    most_popular_articles_report = most_popular_articles_readable(cursor)
    most_popular_authors_report = most_popular_authors_readable(cursor)
    many_errors_days_report = many_errors_days_readable(cursor)
    return "\n\n".join([most_popular_articles_report,
                        most_popular_authors_report, many_errors_days_report])


if __name__ == '__main__':
    news_db = psycopg2.connect(database="news")
    cursor = news_db.cursor()
    report = generated_report(cursor)
    news_db.close()
    print(report)
