# FS-Udacity-Logs-Analysis

Implementation of the "Logs Analysis" project for Udacity's Full Stack Web Developer Nanodegree program. This project includes a python script that generates a report summarizing the statistics from a fake news website - including most popular articles, most popular authors, and which days had the most "errors" using data. See **Project Details** for more information.

## Set up

### VM

First, set up the VM according to the instructions [here](https://classroom.udacity.com/nanodegrees/nd004/parts/51200cee-6bb3-4b55-b469-7d4dd9ad7765/modules/c57b57d4-29a8-4c5f-9bb8-5d53df3e48f4/lessons/5475ecd6-cfdb-4418-85a2-f2583074c08d/concepts/14c72fe3-e3fe-4959-9c4b-467cf5b7c3a0) (must be enrolled in nanodegree program)

### Get news database

Get the [news data](https://d17h27t6h515a5.cloudfront.net/topher/2016/August/57b5f748_newsdata/newsdata.zip) and unzip it within the `/vagrant` directory of your VM. Inside will be a file called `newsdata.sql`. 

Within your VM, `cd` to `\vagrant`. Once inside, connect to the database called `news` (this should already be set up) and run the SQL statements in the `newsdata.sql` file. You can do both by running the following command:

```bash
psql -d news -f newsdata.sql
```

### Set up Views

In the vm, run `psql`. Within the PostgreSQL interpreter, set up the required views by running the following commands.

num_requests_day_logs:
```sql
CREATE VIEW num_requests_day_logs
AS
SELECT
    date_trunc('day', time) AS day,
    count(*) AS num_requests
FROM
    log
GROUP BY
    day;
```
num_errors_day_logs:
```sql
CREATE VIEW num_errors_day_logs
AS
SELECT
    date_trunc('day', time) AS day,
    count(*) AS num_errors
FROM
    log
WHERE
    status LIKE '4%'
    OR status LIKE '5%'
GROUP BY
    day;
```

article_views:
```sql
CREATE VIEW article_views
AS
SELECT
    articles.id,
    count(log.id) AS views
FROM
    articles
    LEFT JOIN log ON log.path = concat('/article/', articles.slug)
GROUP BY
    articles.id;
```

## Usage

After doing the setup, to get the report, simply `cd` into this project (within the VM) and run the following command:
```sh
./report.py
```

Alternatively:
```sh
python2 ./report.py
```

## Project Details

This project uses a VM containing a PostgreSQL database server that we retreive data from. The data we retrieve is information about fake news articles on a website; this includes article titles, authors, information about access requests from users, and other data.

The included `report.py` can be used to generate a text report answering the following questions:
- What are the most popular three articles of all time?
- Who are the most popular article authors of all time?
- On which days did more than 1% of requests lead to errors?

To get the database information for the first and second questions, we first create a view called `article_views`, which acts as an intermediate table that shows how many hits each article has gotten. This is done by counting how many users attempted to access an article, which we accomplish by joining the `articles` table with the `log` table and counting, per article, how many users attempted a `GET` request on an article and received a `200 OK` response.

The `article_views` view is then joined with the `articles` or `authors` table (or both), so we can retrieve the article titles and author names associated with each article. We then either sort the views (in the case of the first question) or sum up views of articles by the same author (in the case of the secod question).

To get the database information for the third question, we create two views `num_requests_day_logs` and `num_errors_day_logs`, which count, per article, the number of HTML requests to that article and the the number of bad HTML responses (4xx or 5xx) respectively. The counts are done by truncating the timestamps in `log` to ignore the time, and then grouping by the truncated timestamps (now just days) to achieve a count by day.

These are then joined together, so we can see date, number of error responses per day, and total number of requests per day at the same time - and then we calculate the percentage of html requests that led to errors.

For the report, we print out the question that each of these database queries answers, followed by the results of the database query in a more human readable format.